#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#define TAM 200
int main() {

	mkfifo("/tmp/mififo_logger",0777);
	int fdread;
	fdread=open("/tmp/mififo_logger",O_RDONLY);

	while (1){
		char buffer[TAM];
		char buffer_previo[TAM];
		int i;
		for(i=0;i<TAM;i++)
			buffer_previo[i]=buffer[i];

		read(fdread,buffer,sizeof(buffer));
			
		if(strcmp(buffer,buffer_previo)!=0)		
			printf("%s \n",buffer);

		if(strcmp(buffer,"FIN")==0)
			break;		

	}
	
	close(fdread);
	unlink("/tmp/mififo_logger");


  return 0;
}
