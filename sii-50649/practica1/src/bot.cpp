//#include <iostream.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "../include/DatosMemCompartida.h"
//#include "../include/Esfera.h"

int main() {
	
	DatosMemCompartida *pdatoscomp;

	char *org;

	int fichero_proyectado;

	fichero_proyectado=open("/tmp/botfichero.txt",O_RDWR);

	org=(char *)mmap(NULL,sizeof(*(pdatoscomp)), PROT_WRITE | PROT_READ, MAP_SHARED, fichero_proyectado,0);
	close(fichero_proyectado);

	pdatoscomp=(DatosMemCompartida *)org;

	while (1){

	if((pdatoscomp->esfera.centro.y-(pdatoscomp->raqueta.y1+pdatoscomp->raqueta.y2)/2)>0)	
		pdatoscomp->accion=1;

	if((pdatoscomp->esfera.centro.y-(pdatoscomp->raqueta.y1+pdatoscomp->raqueta.y2)/2)<0)	
		pdatoscomp->accion=-1;

		usleep(25000);	
	} 
	
	munmap(org,sizeof(*(pdatoscomp)));
	
  return 0;
}
