// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"


#define TAM 200
class CMundoServidor  
{
public:
	
	char mensaje[TAM];
	int fifo;

	char mensaje_de_servidor_a_cliente[TAM];
//	int fifo_de_servidor_a_cliente;
	
	pthread_t thid1,thid_clientes;

//	int fifo_de_cliente_a_servidor;
	char mensaje_de_cliente_a_servidor[TAM];

	int banderita;

	Socket conexion_a_cliente;
	Socket comunicacion_a_cliente;

	Socket servidor;
	std::vector<Socket> conexiones;
	void GestionaConexiones();

	std::vector<char *> nombres;

	char nombrecopia[200];
	char nombrecopia_previo[200];
	char espectadores[200][200];

	int num_esp_prev;
	int flag_mostrar_jugador;
	int acabar;
	
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
