// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
//#include <stdio.h>
#include <string.h>
#include <math.h>


#define PUERTO_SOCKET 4200



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void * hilo_comandos(void * d);
void * hilo_gestion_sockets(void * d);

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	sprintf(mensaje,"FIN");
	write(fifo,mensaje,sizeof(mensaje));
	
	acabar=1;
	
	comunicacion_a_cliente.Close();
	conexion_a_cliente.Close();

	pthread_join(thid1,0);
	pthread_join(thid_clientes,0);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void prints(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	prints(cad,10,0,1,1,1);

	prints(mensaje_de_cliente_a_servidor,120,0,1,1,1);	


	sprintf(cad,"Jugador2: %d",puntos2);
	prints(cad,200,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	
	if(nombres.size()!=num_esp_prev){
		printf("\nJUGADOR\n\n");
		printf(mensaje_de_cliente_a_servidor);
		printf("\n\nEspectadores\n\n");
		if(nombres.size()>1){
			for(int i=1;i<nombres.size();i++){	
			strcpy(espectadores[i],nombrecopia);
			printf(espectadores[i]);
			printf(",");
			}
	
			printf(espectadores[0]);
		}
		else if(nombres.size()!=0){
			strcpy(espectadores[0],nombrecopia);
			printf(espectadores[0]);
		}
		printf("\n");
	}
	else if((nombres.size()==0)&&(flag_mostrar_jugador==1)){
		printf("\nJUGADOR\n\n");
		printf(mensaje_de_cliente_a_servidor);	
		printf("\n");
		flag_mostrar_jugador=0;
	}
	num_esp_prev=nombres.size();

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	
	if(puntos1==5||puntos2==5) exit(0);

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(mensaje,"Jugador 1 lleva %d ::: Jugador 2 lleva %d",puntos1,puntos2);
		write(fifo,mensaje,sizeof(mensaje));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	sprintf(mensaje,"Jugador 1 lleva %d ::: Jugador 2 lleva %d",puntos1,puntos2);
	write(fifo,mensaje,sizeof(mensaje));
	}

	sprintf(mensaje_de_servidor_a_cliente,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
//	write(fifo_de_servidor_a_cliente,mensaje_de_servidor_a_cliente,sizeof(mensaje_de_servidor_a_cliente));

	comunicacion_a_cliente.Send(mensaje_de_servidor_a_cliente,sizeof(mensaje_de_servidor_a_cliente));

	for(i=conexiones.size()-1; i>=0; i--){
		conexiones[i].Send(mensaje_de_servidor_a_cliente,sizeof(mensaje_de_servidor_a_cliente));
		
	/*	if(conexiones[i].Send(mensaje_de_servidor_a_cliente,sizeof(mensaje_de_servidor_a_cliente))){
			conexiones.erase(conexiones.begin()+i);
			if(i<2){  //Hay menos de dos clientes conectados
				puntos1=0;
				puntos2=0;			
			}
		}
	*/

	}	


}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void * hilo_comandos(void* d)
{

	CMundoServidor* p=(CMundoServidor*)d;
	p->RecibeComandosJugador();

}

void * hilo_gestion_sockets(void* d)
{

	CMundoServidor* p=(CMundoServidor*)d;
	p->GestionaConexiones();

}

void CMundoServidor::RecibeComandosJugador()
{
	while (!acabar) {
		usleep(10);

		char cad[100];

		comunicacion_a_cliente.Receive(cad,sizeof(cad));

		unsigned char key;
		sscanf(cad,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;

	}

}

void CMundoServidor::GestionaConexiones()
{
	while (!acabar) {
		char nombre[200];
		Socket auxiliar;
		auxiliar=conexion_a_cliente.Accept();
		conexiones.push_back(auxiliar);
		auxiliar.Receive(nombre,sizeof(nombre));
		nombres.push_back(nombre);
		strcpy(nombrecopia,nombres[nombres.size()-1]);
	}
}

void CMundoServidor::Init()
{
	fifo=open("/tmp/mififo_logger",O_WRONLY);
//-----------------------------------------------------------------------------------------------
	pthread_create(&thid1, NULL, hilo_comandos,this);
	pthread_create(&thid_clientes, NULL, hilo_gestion_sockets,this);
//-----------------------------------------------------------------------------------------------
	conexion_a_cliente.InitServer((char *)"127.0.0.1",4200);

	comunicacion_a_cliente=conexion_a_cliente.Accept();
	comunicacion_a_cliente.Receive(mensaje_de_cliente_a_servidor, sizeof(mensaje_de_cliente_a_servidor));
//-----------------------------------------------------------------------------------------------
	acabar=0;
	num_esp_prev=0;
	flag_mostrar_jugador=1;
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
