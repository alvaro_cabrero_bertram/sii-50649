// MundoCliente.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"


#define TAM 200
class CMundoCliente  
{
public:
	int fichero_proyectado;
	char *org;
	
//	int fifo_de_servidor_a_cliente;
	char mensaje_de_servidor_a_cliente[TAM];

//	int fifo_de_cliente_a_servidor;
	char mensaje_de_cliente_a_servidor[TAM];

	DatosMemCompartida datoscompartidos;
	DatosMemCompartida *pdatoscompartidos;	

	Socket comunicacion_a_servidor;
	
	char nombre_cliente[200];
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
